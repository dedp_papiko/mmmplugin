﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using DxMath;
using Linearstar.MikuMikuMoving.ApplyYobiHnadouPlugin.Properties;
using Linearstar.MikuMikuMoving.Framework;
using MikuMikuPlugin;


//https://sites.google.com/site/mikumikumoving/manual/plugin


namespace Linearstar.MikuMikuMoving.ApplyYobiHnadouPlugin
{
	//CommandBaseである点に注意
	//再度実行時に値が保存されている点に注目
	public class ApplyYobiHnadouCommand : CommandBase
	{		
		int keyFrameInterval	= 0;			//キー化間隔　初期値
		int noiseValueInterval	= 1;			//ノイズ間隔
		int keyShiftWidth		= 0;			//キー幅
		bool isPositionLocal;
		bool isRotationLocal;

		int loopNum = 0;//付け加え loopNumで+して行くところは繰り返しの箇所である必要がある。

		//これは何？　宣言？　getとかセットとか???←ゲットもセットもない単なる初期がっぽい。
		//まずNoiseValueが分からん →他ソース　←　要素数が足りないと思うんですけど。 

		//普通の宣言でした。→　本当にそうなんか？
		//あってました、配列としての入れ込み方法ですわ。　いやそれもおかしいでしょ。
		//NoiseValue　が　structだからこのように書くと推察。
		//個人情報 = { 名前, 年齢, 住所 }
		//＜本来だと、 Hoge hoge = new Hoge();　としておいて、そこから hoge.piyo = 1.0f; hoge.ababa = 3.5f;のようにプロパティを設定したりもするのですが
		//＜おそらくコンストラクタをメソッドで組んで実装すると、Hoge（1,1,5）のようにどの引数がどこに入るか分からない故にこういう記述方法をしているのだと思いますの
		//＜おそらく構造体（Struct）のコンストラクタを初期値付きで呼び出してるのだと思いますが、これはかなり手の込んだ書き方ですのう…（蛇もこの記述方法は知らなかった…
		//オブジェクト初期化子かな 
		//noiseValue.PositionWidth = new Vector3(5,5,5);
		//をずらずら書くのを避けるための書き方

		YobiHnadouValue YobiHnadouValue = new YobiHnadouValue
		{
			PositionWidth	= new Vector3(0, 0, 0),
			RotationWidth	= new Vector3(0, 0, 0),
			GravityWidth	= 10,
			GravityDirectionWidth = new Vector3(1, 1, 1),
		};
		private bool isReversalCheckBox1 = false;//追加

		//1.override　つまりクラスを上書きしている　CommandBaseクラスからのもの
		//2.CommandArgs　これが良くわからんが上記のクラスが絡んでそう。
		//イベントのe？その面注意してみる。
		//void Run(CommandArgs e) ボタンが押されたときに呼ばれます。起動時の処理
		/// <summary>
		/// 本日
		/// </summary>
		/// <param name="e"></param>
		/// ///三つでデル
		public override void Run( CommandArgs e )
		{
			//ここのインスタンスを使いまわすのは　ランダム製の保障
			//新しくつどつどインスタンスを作製して　ランダム値を射出すると　統合的なランダム製を保障する事は出来ないから。
			var r = new Random();
			//SceneのMode
			var mode = this.Scene.Mode;//Scene　はMMM内部の様々なオブジェクトにアクセスすることができます。modeは何？
									   //Mode EditMode 現在の編集モードです。

			//追記　//毎回プラグインボタン押し毎に値を初期化
			YobiHnadouValue = new YobiHnadouValue {
				PositionWidth = new Vector3(0, 0, 0),
				RotationWidth = new Vector3(0, 0, 0),
				GravityWidth = 10,
				GravityDirectionWidth = new Vector3(1, 1, 1),
			};
			keyShiftWidth = 0;          //キー幅

			//構文から分からん
			//多分左の書き方で現在のシーンモードを確定させる事ができるのであろう
			//下記のソースにて確認する。
			//ModelMode　AccessoryMode　EffectMode
			//あとLINQ自体射影とか専門用語が多い。

			var isModelOrAccessoryOrEffect = mode == EditMode.ModelMode || mode == EditMode.AccessoryMode || mode == EditMode.EffectMode;//モード決定。原型
			var isModel = mode == EditMode.ModelMode;
			var isAccessoryOrEffect = mode == EditMode.AccessoryMode || mode == EditMode.EffectMode;//モード決定。

			//LINQ構文から分からん
			//配列っぽい？ linq? 
			//var src = new[] { 3, 2, 9, 6 };
			//var mapped = src.Select(elem => elem * 2); // {6, 4, 18, 12}
			//Selectでますのようにつかえる

			// Cameras	 CameraCollection	 カメラにアクセスします。

			// SelectMany
			// えっとアンダーバーだけのプロパティがあるのかな？・・・困惑。　→　ここのLINQ内で使用される変数になるっぽい。
			//この書き方はLINQかどうか？SelectManyがあるからLINQだな！→LINQえすね。
			//SelectManyメソッドはエンティティから特定のプロパティを取り出すという意味では、
			//Selectメソッドと同じですが、最後の結果をフラット化（平坦化）する点が異なります。
			//平坦化とは
			//Cameras　SceneにCamera　はあるがCamerasはない。
			//とにかく_がと　Cameras　と　平坦化がわからな。
			//>>例は SelectManyの引数に names => names というラムダ式をデリゲートとして渡してます。
			//sはLINQ由来によるものらしい。多分　自信ないけど。
			//カメラの状況にアクセスしていると予測　深くは考えない！！

			//Layers　モーションレイヤ　例　Accessory.Layers　型 MotionLayerCollection アクセサリがもつモーションレイヤ

			//select句は全然書き方が違う　これはselectmany特有　でselectmanyの構文
			//selectmanyは相当難しい　C#のオーバーロード　ラムダ式　リスト　デリゲートが絡んでくる。
			//明確な理解は現状不可能と思われる。平坦化など。
			//http://qiita.com/RyotaMurohoshi/items/81d73fb2cdd751da0a84

			//selectManyの例文
			//List<List<string>> listOfNameList = LoadListOfNameList();
			//List<string> nameList = listOfNameList.SelectMany(names => names).ToList();

			//結局何がしたいのか？
			//多分カメラのレイヤーにアクセスして　カメラモードかどうかの判別をさせている　他は少しフォームへのアクセスのやり取りを行っているが詳細は不明。
			//アンダーバーはSQLの途中の入れ子のようなものでここでしか使用しない　はず。
			//一応isCameraSelectedの使用影響は少ないのでそこまでの追及は必要はない。

			//1個目のSelectManyでCamerasの中のcameraが持っているlayerを全部取り出して
			//2つ目で全部のlayerがもってるSelectedFrameを全部取り出してる
			var isCameraSelected = this.Scene.Cameras.SelectMany(_Camera => _Camera.Layers).SelectMany(_layer => _layer.SelectedFrames).Any();

			//this.Scene.Cameras = _ 

			//http://ideone.com/QtRGvj
			//.Any(); 存在するかどうかを　ブーリアン型で返す。
			//Any 	条件を満たす要素がシーケンス中に1つでもあれば true を返す。

			// プロパティ	SelectedPropertyFrames
			// 型			SelectedModelPropertyFrameCollection
			// 返却値		選択されたプロパティキーフレームにアクセスします。
			//推察するに　選択したキーの判断　つまり　キーが選択されているかどうか
			var isEnvironmentSelected = this.Scene.SelectedPropertyFrames.Any();

			//1.シーンモードが指定のものではない。
			//2.カメラが選択されていない。
			//3.もしくは何もキーが選択されていない場合

			//上記条件を満たした場合エラーメッセが出るようになっている。
			//if (!isModelOrAccessoryOrEffect && !isCameraSelected && !isEnvironmentSelected) {//原型
			if (!isModel || isCameraSelected) {
				//Utilが不明???IEnumerableが使用されているコンテナークラス　
				//ミーフォさんの自作クラス　目的は不明　様々な処理を書いているようだ。
				MessageBox.Show(this.ApplicationForm, Util.Bilingual
				(
				this.Scene.Language,
				"実行はモデル動作のみとなります。\r\n対象のキーフレームを選択してから実行してください。",
				"No target selected.\r\nPlease select one or more keyframes to apply some noise."
				), Util.Bilingual(this.Scene.Language, this.Text, this.EnglishText), MessageBoxButtons.OK, MessageBoxIcon.Information);

				e.Cancel = true;

				return;//これで何もしない処理のようだ。
			}

			//usingがわからん???　他ソースからの読み込みではない　→　http://urashita.com/archives/1825　簡単に言うと　例外処理強制実力実行版のようなもの
			//usingは最後まで実行される。そのため例外処理の記述が少なくて済む？
			//フォームを外部からの読み出しのようだ
			//上部宣言につかわれているかどうか確認。　→上部で宣言していない。
			//引数にフォームをクラスとして読み込み
			//各値をゲッターセッターのように採取しているようだ。
			//
			//そこ、プロパティの宣言か。。。なんちゅうややこしい書き方。
			//
			using (var f = new ApplyYobiHnadouForm//なんというかここでフォーム画面作成しているっぽい。
			{
				//フォームの値を見たらそこから値を採取しているようだ。大文字小文字に注意。
				//noiseValue.PositionWidth = new Vector3(5,5,5);
				//をずらずら書くのを避けるための書き方
				//あれ？isReversalCheckBox1はローカル変数
				//IsReversalCheckBox1はクラス名になるんか？
				//思っているのと逆？
				//ここではこのソース上の初期値をフォームに入れ込んで初期化しているようだ。
				//IsReversalCheckBox1はフォームの何をさしているのか???→普通にプロパティに入れ込みしてるだけ
				//クラス名ではないし・・・　恐らくゲッターセッターの部分による値
				//ゲッターセッターは普通のプロパティ宣言だよ。
				//???これやってしまうと毎回値が初期化されるのではないだろうか

				KeyFrameInterval = keyFrameInterval,
				KeyShiftWidth		 = keyShiftWidth,
				NoiseValueInterval	 = noiseValueInterval,
				YobiHnadouValue			 = YobiHnadouValue,

				IsPositionEnabled		 = isModelOrAccessoryOrEffect || isCameraSelected,
				IsPositionLocalVisible	 = isModelOrAccessoryOrEffect,
				IsRotationEnabled		 = isModelOrAccessoryOrEffect || isCameraSelected,
				IsRotationLocalVisible	 = isModelOrAccessoryOrEffect,
				IsEnvironmentEnabled	 = isEnvironmentSelected,
				IsPositionLocal			 = isPositionLocal,
				IsRotationLocal			 = isRotationLocal,
				IsReversalCheckBox1		 = isReversalCheckBox1,
			})
			{
				//using関数中身
				//e.　が使われていないのが予想外。→中で使われている。
				//f　は　ApplyKaidanForm
				//キャンセル時の処理。
				if (f.ShowDialog() != DialogResult.OK) {
					e.Cancel = true;
					return;
				}

				keyFrameInterval	 = f.KeyFrameInterval;
				noiseValueInterval	 = f.NoiseValueInterval;
				YobiHnadouValue			 = f.YobiHnadouValue;
				keyShiftWidth		 = f.KeyShiftWidth;
				isPositionLocal		 = f.IsPositionLocal;
				isRotationLocal		 = f.IsRotationLocal;
				isReversalCheckBox1	 = f.IsReversalCheckBox1;

				//usingの中でusinが使われている　本気で意味分からん。→一応分かった　なんか幅広い例外処理みたいな感じ。
				//this.Sceneを引数にして状況を判断している
				//undoblocに入る事に不安が発生するが
				//その飛び先にてコメントアウトしているため心配は少なくて済む。
				//恐らくここで一旦引数に入れt状態を保存しているものだとおもわれる。
				using (new UndoBlock(this.Scene))
					switch (this.Scene.Mode)
					{
						//ActiveAccessory
						//Accessory   
						//現在選択中のアクセサリです。選択されていない場合は null が返ります。
						//つまりtrueで中に入るってこと？違う　this.Scene.Mode　=　EditMode.AccessoryModeってこと。
						case EditMode.AccessoryMode:
							e.Cancel = true;//追記
							return;//これで何もしない処理のようだ。//追記
							var acc = this.Scene.ActiveAccessory;

							if (acc != null)//ここでチェック入れる意味がいまいち分からんけど。
								//LINQとforeachの組み合わせ　そして普通に出てくる_　分からん。
								//まずLINQのforeach構文をチェック
								//クエリ式：var q = from x in collection where x > 10 select x * x;
								//メソッド形式で LINQ：var q = collection.Where(x => x > 10).Select(x => x * x);

								//foreach (var 名 in 出席番号前半名)
								//{
								//	Console.Write("{0}\n", 名);
								//}

								//CameraLayer
								//SelectedFrames　選択されたキーフレームです。
								//CameraSelectedFrameCollection

								//ProcessLayer　関数　

								//下記のファンクション
								//foreach　foreach(型名 変数 in コレクション)文
								foreach (var layer in acc.Layers.Where(_ => _.SelectedFrames.Any()))//これは何を対象にぶんまわすのか？//Layers　モーションレイヤ　例　Accessory.Layers　型 MotionLayerCollection アクセサリがもつモーションレイヤ
									ProcessLayer(r, layer.SelectedFrames.First().FrameNumber, layer.SelectedFrames.Last().FrameNumber, layer.Frames, isPositionLocal, isRotationLocal, null);

							break;
						case EditMode.CameraMode:
							e.Cancel = true;//追記
							return;//これで何もしない処理のようだ。//追記

							var camera	= this.Scene.ActiveCamera;						//カメラの操作モードに切り替えるイメージ???　ActiveCamera　 ActiveCamera	 Camera	 現在選択中のカメラです。選択されていない場合は null が返ります。
							var env		= this.Scene.SelectedPropertyFrames;			// SelectedPropertyFrames	SelectedSceneFrameCollection	選択されているプロパティキーフレームにアクセスします。 

							if (camera != null)//別口でアクセス
								foreach (var layer in camera.Layers.Where(_ => _.SelectedFrames.Any()))　//ここのカメラはすぐ上でのカメラであることに注意。
									ProcessCameraLayer(r, layer.SelectedFrames.First().FrameNumber, layer.SelectedFrames.Last().FrameNumber, layer.Frames);

							if (env.Any())//たびたびでるanyは何なんだ。
								ProcessEnvironmentLayer(r, env.First().FrameNumber, env.Last().FrameNumber, this.Scene.PropertyFrames);

							break;
						case EditMode.EffectMode:
							e.Cancel = true;//追記
							return;//これで何もしない処理のようだ。//追記
							var eff = this.Scene.ActiveEffect;

							if (eff != null)
								ProcessLayer(r, eff.SelectedFrames.First().FrameNumber, eff.SelectedFrames.Last().FrameNumber, eff.Frames, isPositionLocal, isRotationLocal, null);

							break;
						case EditMode.ModelMode:
							var model = this.Scene.ActiveModel;

							loopNum = 0;//自分で加えた　ここはモデルの全ボーンを周回しているためここでフレームズラシを加算するのは駄目
							if (model != null) {
								//foreach (変数 in コレクション) { ... }
								//！！！ここをボーンの表示している順にしないと　pmxのボーン順番になるよ！！！@task
								//つまり表示順で取得しないとだめ。しかしその方法はあるのでしょうか？

								//Model >プロパティ名　Bones	 型　BoneCollection	ボーンにアクセスします。
								//BoneCollectionはBoneの集まり
								//Bone　>プロパティ名　Layers	 型　MotionLayerCollection	 ボーンがもつモーションレイヤーを取得します。
								//MotionLayerCollectionはMotionLayerの集まり
								//MotionLayer　Frames 	MotionFrameCollection
								//			 Selected	 bool などある。

								//Model>  プロパティ名DisplayFrame	 型　DisplayFrameCollection　	 表示枠にアクセスします。
								//DisplayFrameCollection　>　 this[int]　	DisplayFrame　 インデックスで表示枠にアクセスします。
								//DisplayFrame　＞ Bones	DisplayFrameBoneCollection　表示枠にあるボーンにアクセスします。
								//DisplayFrameBoneCollection　これがよく分からんす　 GetEnumerator()　反復処理する列挙子を返します。つまり foreach 等が使用できます。とある。

								//model.DisplayFrame.name はあのボーンのｸﾞﾙｰﾌﾟ名が変える。

								//つまり結論的に言うと　Bonesの型が　MotionLayerCollection　と　DisplayFrameBoneCollectionで違う　ガッテム
								//
								//なにかいい方法はないかと思うと　BoneCollection >  this[string]	 Bone	 ボーン名でアクセスします。該当するボーン名が無い場合は null が返ります。
								//とあった。
								//つまり　BoneCollectionからのBoneの指定方法を現在　foreachでｲﾝﾃﾞｯｸｽ順でやっているのをネームを無理やりいれて表示順にすればよろし。

								//SelectedFrames　は　

								//////原型
								//foreach (var bone in model.Bones) {//ここの配列を逆回しできれば一発ですむよ。

								//	foreach (var layer in bone.Layers.Where(_ => _.SelectedFrames.Any())) {
								//		ProcessLayer(r, layer.SelectedFrames.First().FrameNumber, layer.SelectedFrames.Last().FrameNumber, layer.Frames, isPositionLocal, isRotationLocal, bone);
								//		loopNum++;//フレーム数操作の為の変数処理

								//		//ここで値の操作を行うのであれば+マイナス程度になるなぁ。
								//		//しかし、現状では
								//	}//layer＝CameraFrame　＝＞　 Frames＝CameraFrameCollection　＝＞CameraFrameCollection　＝　ICameraFrameData
								//}

								//ここで分かった大事な事
								//コレクションと付いてる配列
								//それをforeachでまわす　（配列設定が複数ある場合は？）//恐らくその型のリファレンスでまわせるどうの書いてると思う
								//まぁ出せるって感じ
								//それぞれの型に注意。またプロパティ名で検索してもろくな目にあわない　プロパティ名が同じで　型が違うし
								//違うクラスで極力合わせているって事もある。

								//

								if (isReversalCheckBox1 == false) {
									//foreachの逆回しって　forにして　カウンターを逆に回すぐらいかな？

									foreach (var disp in model.DisplayFrame) {//ここの配列を逆回しできれば一発ですむよ。
										foreach (var displayFrameBoneCollection in disp.Bones) {//ここの配列を逆回しできれば一発ですむよ。
											string boneName = displayFrameBoneCollection.Name;
											Bone bone = model.Bones[boneName];//これでボーンを指定できる。2時配列？それにしては何故か引き出せるしな。よくわからん。???

											if (bone != null) {
												foreach (var layer in bone.Layers.Where(_ => _.SelectedFrames.Any())) {

													ProcessLayer(r, layer.SelectedFrames.First().FrameNumber, layer.SelectedFrames.Last().FrameNumber, layer.Frames, isPositionLocal, isRotationLocal, bone);
													loopNum++;//フレーム数操作の為の変数処理

													//ここで値の操作を行うのであれば+マイナス程度になるなぁ。
													//しかし、現状では
												}//layer＝CameraFrame　＝＞　 Frames＝CameraFrameCollection　＝＞CameraFrameCollection　＝　ICameraFrameData
											}
										}
									}
								} else if (isReversalCheckBox1 == true) {
									//逆回転バージョンを作製する。
									//一番大外のforを逆回転にする必要がある。
									//var disp in model.DisplayFrame.OrderByDescending(x => x)で頑張ったがだめだった　エラーストップする
									//何かいい方法はないかとLINQで探ったが　this[int]をひっくり返すことができず（また通常では何のリストでよみだしているかもわからん！）?? forecach
									//xの何を？ソートするの？でしょうね　→じゃあ普段foreachは何を対象にまわしてるの？　インデクサっぽい　アイナメラブルっぽい。
									//結局　Reverse()　Enumerableの機能で終わらせた。
									//結局はLINQのthis[int]の返し方はどうするの？っていうのが疑問に残ったままに???
									foreach (var disp in model.DisplayFrame.Reverse()) {//ここの配列を逆回しできれば一発ですむよ。//ここなんでReverse()使ってるのに2回目もう元通りになってるの???
										foreach (var _displayFrameBoneCollection in disp.Bones.Reverse()) {//ここの配列を逆回しできれば一発ですむよ。
											string boneName = _displayFrameBoneCollection.Name;
											Bone bone = model.Bones[boneName];//これでボーンを指定できる。2時配列？それにしては何故か引き出せるしな。よくわからん。???

											if (bone != null) {
												foreach (var layer in bone.Layers.Where(_ => _.SelectedFrames.Any())) {

													ProcessLayer(r, layer.SelectedFrames.First().FrameNumber, layer.SelectedFrames.Last().FrameNumber, layer.Frames, isPositionLocal, isRotationLocal, bone);
													loopNum++;//フレーム数操作の為の変数処理

													//ここで値の操作を行うのであれば+マイナス程度になるなぁ。
													//しかし、現状では
												}//layer＝CameraFrame　＝＞　 Frames＝CameraFrameCollection　＝＞CameraFrameCollection　＝　ICameraFrameData
											}
										}
									}
									//続いてフォームの非表示方法が分からん　




								}
							}
							break;
					}
			}
		}

		//ProcessLayer　→　ProcessFrames　

		//分からん
		//実際のプロセス項目のようだ
		//引数にとりあえず、フォームでの決定した値を全て入れ込み実行しているようだ。
		//この関数が実際に実行される場所は上記のシーン選択からのスイッチ文になる。

		//
		//Random	r							ランダム値ランダムクラス
		//long		beginFrame,					開始フレーム
		//long		endFrame,					終了フレーム
		//MotionFrameCollection	frames,			MotionFrameCollectionというクラスについて　キーの配列みたいなものかな　リファレンスに書いてる
		//bool		isPositionLocal,			座標
		//bool		isRotationLocal,			回転
		//Bone		localBone					ローカルボーン　Bone　元は　model.Bones　マジvar勘弁　Scene.ActiveModel　ActiveModel	 Model	 現在選択中のモデルです。選択されていない場合は null が返ります。
		//プロセスレイヤー　うーん分からん。　フレームを実際に動かすのは別の関数。
		//モデル部分に使われているのはこの関数
		/// <summary>
		/// 
		/// </summary>
		/// <param name="r"></param>
		/// <param name="beginFrame"></param>
		/// <param name="endFrame"></param>
		/// <param name="frames"></param>
		/// <param name="isPositionLocal"></param>
		/// <param name="isRotationLocal"></param>
		/// <param name="localBone"></param>
		void ProcessLayer( Random r, long beginFrame, long endFrame, MotionFrameCollection frames, bool isPositionLocal, bool isRotationLocal, Bone localBone  )
		{
			//varでクラスも入るのだ！　ProcessFramesは自作関数で下部に表記。
			//下部の書き方よく分からん　特に　_ => _.FrameNumber
			//下記はこのソース最難関部分　関数使用で引数にラムダ式にLINQを使っている！もう助けて！→　ラムダ式使ってなくね？　多分使ってる。　new HashSet<long>がそう　これが引数の値として
			//実行されていると予測する。
			//HashSet　に関してはMMMプラグインリファレンス検索にもかからなかった。ハッシュという概念がいまだによくわかっていない。
			//HashSet<T> はC#の標準関数のようだ。　→　順序は狂っても構わないので、 要素の検索だけ高速にできてほしい場合があって、 そういうコレクションのことをセット（set: もちろん、数学的な意味での集合のこと）と呼びます。
			//ラムダ付近の主語としては　何を返すのがポイントにあんる　そしてそのラムダに対してLINQをかけているのがまじで謎→直接はラムダは絡まなかった。
			//まずは問題を分離して｛｝の中身をじっくり読んでみる。特に何を返しているのかに注目して行う
			//現状の予測としては　値で返す？（配列チックな）いや違うLONｇだわ　そしlongに対してLINQを行っているのか？
			//LINQセレクトの基本構文　var mapped = src.Select(elem => elem * 2); // {6, 4, 18, 12}
			//ラムダ基本構文　Func<int, int, int> f =  (x, y) =>  {  return sum * prod; };
			//HashSet<long> existingFrames　　MotionFrameCollection　　FrameNumber　＝　//


			//MotionFrameCollection

			//ProcessFrames(Random r, long beginFrame, long endFrame, int keyFrameInterval, int noiseValueInterval, HashSet < long > existingFrames)
			//MotionFrameCollectionのFrameNumberを探す　//_motionFrame　になった理由　MotionFrameCollection　でもなく　framesでも無い理由　LINQの検索の中ですからね。collectionの中身になるんですわ。　MotionFrame.SelectedがTrueのものをWHEREして選択したlongをここで返している。
			//コメント書き換え　SelectedとなっているFrameをとってきて、そのFrameのFrameNumberをHashSet<long>で取得

			//ProcessFrames　はIEnumerable<KeyValuePair<long, YobiHnadouValue>>を返す。
			//加工されたフレーム情報郡を返す。
			//そしてそれをセレクトで絞る
			//vsのホバーでdllファイルであろうとルートは探れる。めちゃ大事。

			//2回目の条件搾りに関して
			var newKeyFrames = ProcessFrames(r, beginFrame, endFrame, keyFrameInterval, noiseValueInterval , new HashSet<long>(frames.Where(_motionFrame => _motionFrame.Selected).Select(_motionFrame => _motionFrame.FrameNumber) ))//ここでProcessFramesの終了である事が重要
			.Select(_ =>//ここのKeyValuePairであっているが確認重要??　ProcessFramesの帰り値だしね。<KeyValuePair<long, YobiHnadouValue>>　YobiHnadouValueは
			{
				//元　MotionFrameCollection
				//メソッド　GetFrame(long)			型		MotionFrameData 
				//指定フレーム位置のフレーム情報（コピー）を取得します。//キーフレーム間の場合は、補間された値が取得されます。

				//var list1 = list.Where(p => p <= 15).Select(p => p.Name);
				//p <= 15に該当する部分で　｛｝の中身は15に相当する部分をラムダ式で作っている。　ここでラムダが使えないと結構不便だと思う。

				//.Keyが何を指している分からない。　→　KeyValuePairクラスのkeyプロパティ　つまりC#の組み込み
				//.Key　キー/値ペア内のキーを取得します。
				//Type: TKey　keyが何の型なのか　配列なのかわからん。
				//https://msdn.microsoft.com/ja-jp/library/ms224760%28v=vs.110%29.aspx
				//
				var baseFrame = frames.GetFrame(_.Key);												//frames　が自作関数なのか　MMM標準なのか分からない。→　引数だよ!　MotionFrameCollection　//GetFrame

				//とりあえずローカルローテーションをトル方向で理解する。
				var localRotation = GetLocalRotation(isRotationLocal, localBone, _.Value.RotationWidth, _.Value.RotationQuaternion);//何故ローカルローテーション　//Quaternionの型

				//こちら変更　して　搾りの条件文の所で変更しても大丈夫なのかというのが最大の疑問
				//ここで値を変更するものだとってっきり思っていた。
				//FrameNumberってなんだよ　MotionFrameCollectionの要素の一つ　の　MotionFramedateっぽいな　→　 フレーム位置を取得・設定します。long型であることに注意。
				//baseFrame.FrameNumber = Math.Max(_.Key + GetNoise(r, keyShiftWidth), 0);			//@TODO　ここにズラサナイような変更が必要。
				baseFrame.FrameNumber = Math.Max(_.Key + ( keyShiftWidth * loopNum), 0);			//@TODO　hennkou ??ここでフレーム数触っていいんすっか？→　関数部分で触るの値の部分であり　フレームズラシはここで行っているっぽい

				//これは恐らくチェックボックス
				//チェックボックスはいっていると　値の取得先が変更される
				//baseFrame.Quaternion　→　_.Value.RotationQuaternion
				//_.Value.RotationQuaternion　→　baseFrame.Quaternion
				//となっていた　肝心の関数等の説明
				//DXmath　Quaternion　してもリファレンスなし　型もわからず
				//Multiply
				baseFrame.Quaternion = isRotationLocal ? Quaternion.Multiply(baseFrame.Quaternion, _.Value.RotationQuaternion) : Quaternion.Multiply(_.Value.RotationQuaternion, baseFrame.Quaternion);
				baseFrame.Position += isPositionLocal ? GetLocalPosition(isPositionLocal, localBone, _.Value.PositionWidth, baseFrame.Quaternion) : _.Value.PositionWidth;

				baseFrame.Selected = true;		//選択されているフレーム

				//
				return baseFrame;				//リターンかましているからさすがにラムダ臭がする。→LINQのラムダ
			})
			.ToArray(); //ここに注目　anyじゃないんかい！　ToArray　List<T> の要素を新しい配列にコピーします。　→　何にコピーするの???newKeyFramesだろ

			//http://qiita.com/RyotaMurohoshi/items/740151bd772889cf07de　LINQとラムダ。
			//foreach　１行書きみたいなのみたい。

			//_ => はLinqっぽい。
			//MotionFrameCollection
			//
			//指定フレーム位置のキーフレームを削除します。 
			// RemoveKeyFrame(long)
			// _Selectedは一体何をさすのか？
			//疑問_　は上部で指定されたものが関係するのか？
			//上部LINQの影響を受けているのか？
			//ホバーしてみるとIMotionFrameDate　どこから来ているのか？frames（MotionFrameCollection）からのようだ
			//IMotionFrameDataはMotionFrameCollectionが全部実装してるのでは
			//_がMotionFrameCollection（コレクションってついてるから配列）
			//それをforでまわしている。
			//全部削除　ツーかここで削除もやってるんね。
			//この時点で削除しきれていない。
			//ここの_は上記のframesのフレームなのか　ラムダ式なのか　フォーカスが分からない。???
			//多分違うな。　これは引数のものと判断する　???
			//ではますます分からない　古いｷｰフレームはここで全て削除しているはずなのに
			//まずは添付されている情報がおかしくなっている可能性があるので
			//添付せずに表示させてみる。（デバッグしながら表示を見ることができないため。）
			//添付するデータが悪いか　削除が悪いかを検証（下部の添付部をコメントアウトして実行）
			//結果ｷｰフレームが残り　全てのｷｰフレームが削除できていない事が分かった
			//

			//↓これ実は参照渡しなんです！！！①
			var framesXXX = frames.Where(_ => _.Selected).Select(_ => _.FrameNumber);
			int test = 0;

			//②そのため↓が配列でまわしているのに途中で消えていく。
			//foreach (var i in frames.Where(_ => _.Selected)) { 
			//	frames.RemoveKeyFrame(i.FrameNumber);//これでリスト自身が消滅していく
			//	test++;
			//}

			//③このような処理のときLINQはどのように処理するの？

			//framesXXX = frames.Where(_ => _.Selected).Select(_ => _.FrameNumber);

			//var aaaa = frames.Where(_ => _.Selected).ToList();
			//frames.ToList().RemoveAll(x => x.Selected);//④これは参照を消すだけ


			//配列（long）のコピーを作製する（参照避け）
			long[] FrameNumberArray = framesXXX.ToArray();
			//でそれでまわす。
			foreach (var i in FrameNumberArray) {
				frames.RemoveKeyFrame(i);
				test++;
			}



			//AddKeyFrameはキーフレームを加える処理だろうね。MotionFrameCollection大事！
			//IEnumerable<T> から List<T> を作成します。
			frames.AddKeyFrame(newKeyFrames.ToLookup(_ => _.FrameNumber)
			.Select(_ => _.Last())
			.ToList());
		}


		//分からん
		//Layerの概念が分かりません。→　layerはMMMです。
		//カメラレイヤー時に使用。//プロセスの環境レイヤ
		void ProcessEnvironmentLayer( Random r, long beginFrame, long endFrame, SceneFrameCollection frames )
		{
			//ここ超絶長い。まず関数を使用。そしてその関数の中でLINQ　そしてその関数の実行結果をLINQでさらに絞込み　そしてそのLINQの中で｛｝が入っている　その中身が文法的に分からない。
			var newKeyFrames = ProcessFrames(r, beginFrame, endFrame, keyFrameInterval, noiseValueInterval, new HashSet<long>(frames.Where(_ => _.Selected).Select(_ => _.FrameNumber)))
			.Select(_ =>
			{	//ここの{の意味が分からないがLINQの中を｛｝で囲む事ができるのか？
				var baseFrame = frames.GetFrame(_.Key);

				baseFrame.FrameNumber = Math.Max(_.Key + GetNoise(r, keyShiftWidth), 0);//ソース変更　→が元。
				baseFrame.Gravity += _.Value.GravityWidth;
				baseFrame.GravityDirection += _.Value.GravityDirectionWidth;
				baseFrame.Selected = true;

				return baseFrame;
			})
			.ToArray();

			foreach (var i in frames.Where(_ => _.Selected))
				frames.RemoveKeyFrame(i.FrameNumber);

			frames.AddKeyFrame(newKeyFrames.ToLookup(_ => _.FrameNumber)
			.Select(_ => _.Last())
			.ToList());
		}

		//分からん
		//カメラレイヤーの操作時に使用するもの　通常のボーンと同じでは駄目。
		void ProcessCameraLayer( Random r, long beginFrame, long endFrame, CameraFrameCollection frames )
		{
			//ラムダ式というのは無名関数ということからひらめいた　恐らく　｛｝の中身は無名の関数なのだ。
			//ではその関数の中身の書き方。 何をどのような書き方なのかサッパリわかりんす　（メソッド構文）
			//だってここリターンまでつ勝てますよ。
			var existing = frames.ToList();
			var newKeyFrames = ProcessFrames(r, beginFrame, endFrame, keyFrameInterval, noiseValueInterval, new HashSet<long>(frames.Where(_ => _.Selected).Select(_ => _.FrameNumber)))
			.Select(_ =>
			{
				var baseFrame = frames.GetFrame(_.Key);

				baseFrame.FrameNumber = Math.Max(_.Key + GetNoise(r, keyShiftWidth), 0);
				baseFrame.Position += _.Value.PositionWidth;
				baseFrame.Angle += MathHelper.ToRadians(_.Value.RotationWidth);
				baseFrame.Selected = true;

				return baseFrame;
			})
			.ToArray();

			foreach (var i in frames.Where(_ => _.Selected))
				frames.RemoveKeyFrame(i.FrameNumber);

			frames.AddKeyFrame(newKeyFrames.ToLookup(_ => _.FrameNumber)
			.Select(_ => _.Last())
			.ToList());
		}

		
		//ProcessFrames　フレームを加工する？　きーを製作して設置　モデルでのキー操作部分で呼び出し
		//構文が相当分けわかめ　特に　<KeyValuePair<long, NoiseValue>> とかよくわからんし
		//IEnumerableからするとコルーチン？　しかしコルーチンでまわす意味が分からん。
		//→　IEnumerableはLINQで扱うための処理っぽい　インターフェース
		//
		//実際のキー操作をあつかうところっぽい
		//上部二つで使っているからこちらを先に読む。

		//KeyValuePair　longに対応するYobiHnadouValueの値をペアとして保存する型　//連想配列でいれる2次配列みたいなイメージ
		//下部は日本語で書いてみる。
		//これはLINQで利用するため　IEnumerableをインターフェースを導入して
		//そのインターフェーすが2重ジェネリックって感じなのか　→違う　ラムダ式の文法
		//ヤッパリ　IEnumerable<KeyValuePair<long, NoiseValue>>の部分の理解のために　IEnumerableをもう少し詳しく調べてみる。
		//そもそも＜＞がジェネリックなのがか2度目の疑問。
		//自作コンテナの名前を思いっきり乗っけるっぽいね。
		//デリゲート分っぽいな！
		// HashSet<long>はラムダとか関係ないっぽいな。
		//IEnumerableを使う例文をとりあえず

		//HashSet<T>　値のセットを表します。

		//YobiHnadouValueの型でを入れ込む？　とりジェネリック。
		IEnumerable<KeyValuePair<long, YobiHnadouValue>> ProcessFrames( Random r, long beginFrame, long endFrame, int keyFrameInterval, int noiseValueInterval, HashSet<long> existingFrames )
		{
			//Random rは具体的に何ナノか？本当に普通のクラスっぽいな。一番の疑問はランダムをわざわざ引数としてやり取りする点である。書く関数にて生成して行えばいいのに
			//考えられるのは　ノイズ幅の指定の件　と　ランダムシードの部分だが　同一条件でやったとしてもかわならないしなぁ。
			//少しクラスを見てみよう。



			//ここの構文もとんでもなく分からん。＜＞で中身　型となんかのクラスでっせ。
			//それを宣言して　変数をつくあったんだろうけど　その中身に入るのが　なんなのか　currentFrame　なのか　=>　なのか分からん過ぎる。
			//NoiseValue　で宣言して引数いれてる？
			//ラムダ式例文
			//Func<int, int> square = x => x * x;
			//
			//Func<int, int, int> f =
			//( x, y ) =>
			//{
			//	int sum = x + y;
			//	int prod = x * y;
			//	return sum * prod;
			//};

			//currentFrameは引数を入れ込む変数　引数の対象名　これわざわざつけるのは意味あるのかな。
			//getNoiseValueはここで作った一時的変数　_に相当？　→　関数名
			//NoiseValueは何　→　他ﾍﾟｰｼﾞの自作クラス。　それに引数を入れていると思いきや（）が｛｝の悲しみ。
			// Func<int, int, int> Sum = (x, y) => x + y;

			//引数が1項不足している　それがわからんと思っていたがそれがラムダっぽい。Func<int, int, int>となっているうち、<>内の一番右が、戻り値の型になり、それ以外は引数の型になります。
			//
			//longはなんとビックリ帰り値の型ですわ。　そして何に変えるといえば　currentFrameにカエルッポイ。フレームもlongだからオッケーかな。

			//そして｛｝の中で処理を行っている

			//では例文にない　new　Noisevalueは？
			//これが処理の開始っぽい。配列としての入れ込み方法ですわ。だから｛｝

			//でここでgetNoiseValue関数が作製された　これを以下の文で普通に関数として使用する。という形。
			//んーん　普通に関数作ったほうがええんとちゃうんか・・・・
			//さらに申し上げれば　下記の場合　普通に引数のところで計算しているしなー　と思いきやそれを毎行するわけにも行かんからな。便利なのか？
			//currentFrameは引数です！　前のFUNCの。
			//ゲットノイズはオーバーロードが多いのです。

			//オブジェクト初期化子とラムダ式の｛｝を見分けろ

			//static private GetNoiseValue(long currentFrame){
			//	var x = new YobiHnadouValue(){
			//		～ 初期化の式～
			//	};
			//	return x;
			//}

			//currentFrame　＝　YobiHnadouValue　

			//Func<long, YobiHnadouValue> getNoiseValue = GetNoiseValue;
			Func<long, YobiHnadouValue> getNoiseValue = currentFrame => new YobiHnadouValue {
				//多分だけどラムダ式で　（）では無く｛｝だからね。→　違います。
				//オブジェクト初期化子
				//rは幅か？
				//noiseValueはフォームで入力した値！！！このファンクではいきなり出てくる。
				//PositionWidth			 = GetNoise(r, noiseValue.PositionWidth),			//これPositionWidth何処にもつかわれて無いやんと思いきや　YobiHnadouValueのゲッター
				//RotationWidth			 = GetNoise(r, noiseValue.RotationWidth),
				//書き換え
				PositionWidth = GetKaidanWeight(r, YobiHnadouValue.PositionWidth, loopNum),          //これPositionWidth何処にもつかわれて無いやんと思いきや　YobiHnadouValueのゲッター
				RotationWidth = GetKaidanWeight(r, YobiHnadouValue.RotationWidth, loopNum),


				GravityWidth = GetNoise(r, YobiHnadouValue.GravityWidth),			//noiseValue.GravityWidth 重力幅だが実はキーフレーム???　//オーバーロードはソースの追跡がし難くなるな。　floatなのでlongであるフレームとは違うようだ。
				GravityDirectionWidth	 = GetNoise(r, YobiHnadouValue.GravityDirectionWidth),	//DirectionWidth　これもよくわからんけどキーフレームの値なのかな???
			};

			var nextKeyFrame			 = beginFrame + keyFrameInterval;					//keyFrameIntervalは引数　多分keyFrameIntervalは間隔なので　beginFrameを足す必要がある。
			var nextNoiseFrame			 = beginFrame + noiseValueInterval;					//noiseValueIntervalは引数
			var currentNoiseFrame		 = beginFrame;										//beginFrame //ここは不要と判断
			var currentNoiseValue		 = getNoiseValue(currentNoiseFrame);				//ここらへん要る？
			var nextNoiseValue			 = getNoiseValue(nextNoiseFrame);					//ここらへん要る？　初期設計？


			//多分ここでの基礎はフレーム単位。
			//範囲内全て設置。
			for (var i = beginFrame; i <= endFrame; i++) {
				//まず初回
				if (i >= nextNoiseFrame) {
					// update noise value//ここが初期フレームと　エンドフレーム書き換えの原因である。
					currentNoiseFrame = i;
					currentNoiseValue = nextNoiseValue;
					nextNoiseValue = getNoiseValue(nextNoiseFrame += noiseValueInterval);
				}

				//次中間
				if (keyFrameInterval == 0) {			//普通の中部分。
														// overwrite exisitng key frame only//既存のキーフレームのみを上書き
					if (existingFrames.Remove(i)) {		//0フレームにこの処理を加えるとバグル。
														//Interpolate(YobiHnadouValue nextValue, float amount)
						yield return new KeyValuePair<long, YobiHnadouValue>(i, currentNoiseValue.Interpolate(nextNoiseValue, (float)(i - currentNoiseFrame) / (nextNoiseFrame - currentNoiseFrame)));
					}

				} else if (i >= nextKeyFrame || i == beginFrame) {
					//次のキーフレームMaxと開始フレーム画が同じでも発動。
					// create key frame　//0からキーフレムを製作するようだ。
					yield return new KeyValuePair<long, YobiHnadouValue>(i, currentNoiseValue.Interpolate(nextNoiseValue, (float)(i - currentNoiseFrame) / (nextNoiseFrame - currentNoiseFrame)));

					if (i > beginFrame) {
						nextKeyFrame += keyFrameInterval;
					}
				}
			}
		}


		//ローカルのチェックボックス　イン時の差分と思われる。→ではなさそう　そうでもないチェックボックスフラグがここにある。
		//ローカル　マトリックス
		//この下の関数直下の場所のみで使用されている。
		static Matrix GetLocalMatrix( Bone bone )
		{
			if ((bone.BoneFlags & BoneType.LocalAxis) == 0 &&
			bone.LocalAxisX == Vector3.UnitX &&
			bone.LocalAxisY == Vector3.UnitY &&
			bone.LocalAxisZ == Vector3.UnitZ)
				return Matrix.Identity;

			return Matrix.LookAtLH(Vector3.Zero, bone.LocalAxisZ, bone.LocalAxisY);
		}

		//ローカルのチェックボックス　イン時の差分と思われる。
		//ローカルポジション
		static Vector3 GetLocalPosition( bool isLocal, Bone bone, Vector3 position, Quaternion quaternion )
		{
			if (!isLocal)
				return position;

			var rt = Vector3.Transform(bone == null ? position : Vector3.TransformCoordinate(position, GetLocalMatrix(bone)), quaternion);

			return new Vector3(rt.X, rt.Y, rt.Z);
		}

		//ローカルのチェックボックス　イン時の差分と思われる。
		//ローカルの回転
		static Quaternion GetLocalRotation( bool isLocal, Bone bone, Vector3 rotation, Quaternion quaternion )
		{
			if (!isLocal ||
			(bone.BoneFlags & BoneType.LocalAxis) == 0 &&
			bone.LocalAxisX == Vector3.UnitX &&
			bone.LocalAxisY == Vector3.UnitY &&
			bone.LocalAxisZ == Vector3.UnitZ)
				return quaternion;

			return Quaternion.Multiply(Quaternion.Multiply(Quaternion.RotationAxis(bone.LocalAxisY, rotation.Y), Quaternion.RotationAxis(bone.LocalAxisX, rotation.X)), Quaternion.RotationAxis(bone.LocalAxisZ, rotation.Z));
		}

		//_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/
		//回転？移動？どのノイズ？
		//ベクター３のノイズを製作（？）しかし引数にノイズで入っている。
		//これはRandomのクラスの形を理解しないと駄目？→理解した。
		//オーバーロード
		//widthはVector3でDxmathに組しているもの
		//Dxmathの内容を確認したいのだがその方法が分からない
		//また元の値は入っているのかどうかが結構問題
		//ローテーションもポジションも同じ型に入っているけど大丈夫？
		//unityなら6つに分かれていたよね。

		static Vector3 GetNoise( Random r, Vector3 width )
		{

			return new Vector3(GetNoise(r, width.X), GetNoise(r, width.Y), GetNoise(r, width.Z));
		}


		//ベクター３の少数点のノイズを製作（？）しかし引数にノイズが入っている。
		//しかし型がノイズである。　う～ん意味分からん。
		//オーバーロード
		//変数を入れ込めばそれで終わりなのだが
		//staticである事が弊害となっている。
		//staticである必要性ってなんかあるの？
		//因みにここのクラス内でしか使用されていない。
		static float GetNoise( Random r, float width )
		{
			return (float)(r.NextDouble() * width - width / 2 );
		}

		//イント値のランダム 恐らくキーのずらしと推察。→キーフレームのずらしでは無い可能性が高い。
		//オーバーロード
		static int GetNoise( Random r, int width )
		{
			return r.Next(-width / 2, (int)(width / 2.0 + 0.5));
		}
		//_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/

		//●●●●●●●●●●●●●●●●●●●●●●●●●●●●●●//
		//上記ランダムの階段ウェイト版
		//ローテーションもポジションも同じオーバーライドで処理する
		//staticを外す。
		//ここのｘｙｚの値が　float　もしくは　intになるようだ。　もしかして回転ってintのみ？それはない。
		//
		Vector3 GetKaidanWeight( Random r, Vector3 width ,int loopNum ) {

			return new Vector3(GetKaidanWeight(r, width.X, loopNum), GetKaidanWeight(r, width.Y, loopNum), GetKaidanWeight(r, width.Z, loopNum));
		}


		//ベクター３の少数点のノイズを製作（？）しかし引数にノイズが入っている。
		//しかし型がノイズである。　う～ん意味分からん。
		//オーバーロード
		//変数を入れ込めばそれで終わりなのだが
		//staticである事が弊害となっている。
		//staticである必要性ってなんかあるの？
		//因みにここのクラス内でしか使用されていない。
		//widthは幅になるのかな。とりあえずフォーム設定した値がはいるっぽい
		float GetKaidanWeight( Random r, float width, int loopNum ) {
			//return (float)(r.NextDouble() * width - width / 2);
			return (float)(Math.Abs(loopNum) * width);
		}

		//イント値のランダム 恐らくキーのずらしと推察。→キーフレームのずらしでは無い可能性が高い。
		//オーバーロード
		int GetKaidanWeight( Random r, int width, int loopNum ) {
			//return r.Next(-width / 2, (int)(width / 2.0 + 0.5));
			return loopNum * width;

		}



		//●●●●●●●●●●●●●●●●●●●●●●●●●●●●●●//


		//インターフェース
		//ボタンに表示するアイコン画像です。大きさは32x32です。
		//nullを返すとデフォルト画像が表示されます。
		public override Image Image
		{
			get
			{
				return Resources.ApplyNoise32;
			}
		}

		//インターフェース
		//中コマンドバーのボタンに表示するアイコン画像です。大きさは32x32です。
		//nullを返すとデフォルト画像が表示されます。
		public override Image SmallImage
		{
			get
			{
				return Resources.ApplyNoise20;
			}
		}

		//インターフェース
		//プラグインの説明文です。
		//プラグインボタンのツールチップに表示されます。
		public override string Description
		{
			get
			{
				return "YobiHnadou";
			}
		}

		//インターフェース
		//日本以外の環境で表示されるテキストです。
		//改行する場合は Environment.NewLineを使用してください。
		public override string EnglishText
		{
			get
			{
				return Util.EnvorinmentNewLine("Apply\r\nYobiHnadou");
			}
		}

		//インターフェース
		//日本語環境でボタンに表示されるテキストです。
		//改行する場合は Environment.NewLineを使用してください。
		public override string Text
		{
			get
			{
				return Util.EnvorinmentNewLine("YobiHnadou\r\n付加");
			}
		}

		//インターフェース
		//プラグインを一意に認識するためのGUIDです。
		//必ず設定してください。
		public override Guid GUID
		{
			get
			{
				return new Guid("3c92a157-b847-4eb6-9e2d-a1df9786dfcf");
			}
		}


		//インターフェースだが必須ではない

		//--Guid GUID { get; }
		//プラグインを一意に認識するためのGUIDです。
		//必ず設定してください。

		//Win32Window ApplicationForm { get; set; }
		//MMM本体フォームのHWND取得インターフェースです。
		//ShowDialog等に使用してください。

		//Scene Scene { get; set; }
		//Sceneオブジェクトです。
		//これを使用してMMM内部のオブジェクトにアクセスします。

		//string Description { get; }
		//プラグインの説明文です。
		//プラグインボタンのツールチップに表示されます。

		//--Image Image { get; }
		//ボタンに表示するアイコン画像です。大きさは32x32です。
		//nullを返すとデフォルト画像が表示されます。

		//--Image SmallImage { get; }
		//中コマンドバーのボタンに表示するアイコン画像です。大きさは32x32です。
		//nullを返すとデフォルト画像が表示されます。

		//--string Text
		//日本語環境でボタンに表示されるテキストです。
		//改行する場合は Environment.NewLineを使用してください。

		//--string EnglishText
		//日本以外の環境で表示されるテキストです。
		//改行する場合は Environment.NewLineを使用してください。

		//--void Run(CommandArgs e)
		//ボタンが押されたときに呼ばれます。
		//ここに処理を記述します。

		//void Dispose()
		//プラグインが破棄されるときに呼ばれます。
		//解放しないといけないオブジェクトがある場合は、ここで解放してください。
	}
}
