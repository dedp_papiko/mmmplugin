﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using DxMath;
using Linearstar.MikuMikuMoving.ApplyNoisePlugin.Properties;
using Linearstar.MikuMikuMoving.Framework;
using MikuMikuPlugin;


//https://sites.google.com/site/mikumikumoving/manual/plugin


namespace Linearstar.MikuMikuMoving.ApplyNoisePlugin
{
	//CommandBaseである点に注意
	//再度実行時に値が保存されている点に注目
	public class ApplyNoiseCommand : CommandBase
	{
		int keyFrameInterval	= 5;			//キー化間隔　初期値
		int noiseValueInterval	= 5;			//ノイズ間隔
		int keyShiftWidth		= 0;				//キー幅
		bool isPositionLocal;
		bool isRotationLocal;

		//これは何？　宣言？　getとかセットとか???←ゲットもセットもない単なる初期がっぽい。
		//まずNoiseValueが分からん →他ソース　←　要素数が足りないと思うんですけど。
		//普通の宣言でした。→　本当にそうなんか？
		//あってました、配列としての入れ込み方法ですわ。　いやそれもおかしいでしょ。
		//NoiseValue　が　structだからこのように書くと推察。
		//個人情報 = { 名前, 年齢, 住所 }
		//＜本来だと、 Hoge hoge = new Hoge();　としておいて、そこから hoge.piyo = 1.0f; hoge.ababa = 3.5f;のようにプロパティを設定したりもするのですが
		//＜おそらくコンストラクタをメソッドで組んで実装すると、Hoge（1,1,5）のようにどの引数がどこに入るか分からない故にこういう記述方法をしているのだと思いますの
		//＜おそらく構造体（Struct）のコンストラクタを初期値付きで呼び出してるのだと思いますが、これはかなり手の込んだ書き方ですのう…（蛇もこの記述方法は知らなかった…
		
		NoiseValue noiseValue = new NoiseValue
		{
			PositionWidth	= new Vector3(5, 5, 5),
			RotationWidth	= new Vector3(5, 5, 5),
			GravityWidth	= 10,
			GravityDirectionWidth = new Vector3(1, 1, 1),
		};

		//1.override　つまりクラスを上書きしている　CommandBaseクラスからのもの
		//2.CommandArgs　これが良くわからんが上記のクラスが絡んでそう。
		//　イベントのe？その面注意してみる。
		//void Run(CommandArgs e) ボタンが押されたときに呼ばれます。起動時の処理
		public override void Run( CommandArgs e )
		{
			var r = new Random();
			//SceneのMode
			var mode = this.Scene.Mode;//Scene　はMMM内部の様々なオブジェクトにアクセスすることができます。modeは何？
									   // Mode EditMode 現在の編集モードです。

			//構文から分からん
			//多分左の書き方で現在のシーンモードを確定させる事ができるのであろう
			//下記のソースにて確認する。
			//ModelMode　AccessoryMode　EffectMode
			//あとLINQ自体射影とか専門用語が多い。
			var isModelOrAccessoryOrEffect = mode == EditMode.ModelMode || mode == EditMode.AccessoryMode || mode == EditMode.EffectMode;//モード決定。

			//LINQ構文から分からん
			//配列っぽい？ linq? 
			//var src = new[] { 3, 2, 9, 6 };
			//var mapped = src.Select(elem => elem * 2); // {6, 4, 18, 12}
			//Selectでますのようにつかえる

			// Cameras	 CameraCollection	 カメラにアクセスします。

			// SelectMany
			// えっとアンダーバーだけのプロパティがあるのかな？・・・困惑。　→　ここのLINQ内で使用される変数になるっぽい。
			//この書き方はLINQかどうか？SelectManyがあるからLINQだな！→LINQえすね。
			//SelectManyメソッドはエンティティから特定のプロパティを取り出すという意味では、
			//Selectメソッドと同じですが、最後の結果をフラット化（平坦化）する点が異なります。
			//平坦化とは
			//Cameras　SceneにCamera　はあるがCamerasはない。
			//とにかく_がと　Cameras　と　平坦化がわからな。
			//>>例は SelectManyの引数に names => names というラムダ式をデリゲートとして渡してます。
			//sはLINQ由来によるものらしい。多分　自信ないけど。
			//カメラの状況にアクセスしていると予測　深くは考えない！！

			//Layers　モーションレイヤ　例　Accessory.Layers　型 MotionLayerCollection アクセサリがもつモーションレイヤ


			//select句は全然書き方が違う　これはselectmany特有　でselectmanyの構文
			//selectmanyは相当難しい　C#のオーバーロード　ラムダ式　リスト　デリゲートが絡んでくる。
			//明確な理解は現状不可能と思われる。平坦化など。
			//http://qiita.com/RyotaMurohoshi/items/81d73fb2cdd751da0a84

			//selectManyの例文
			//List<List<string>> listOfNameList = LoadListOfNameList();
			//List<string> nameList = listOfNameList.SelectMany(names => names).ToList();

			//結局何がしたいのか？
			//多分カメラのレイヤーにアクセスして　カメラモードかどうかの判別をさせている　他は少しフォームへのアクセスのやり取りを行っているが詳細は不明。
			//アンダーバーはSQLの途中の入れ子のようなものでここでしか使用しない　はず。
			//一応isCameraSelectedの使用影響は少ないのでそこまでの追及は必要はない。
			var isCameraSelected = this.Scene.Cameras.SelectMany(_ => _.Layers).SelectMany(_ => _.SelectedFrames).Any();

			// SelectedPropertyFrames
			// SelectedModelPropertyFrameCollection
			// 選択されたプロパティキーフレームにアクセスします。
			var isEnvironmentSelected = this.Scene.SelectedPropertyFrames.Any();

			//シーンモードが指定のものではない。もしくは何も選択されていない場合
			//エラーメッセが出るようになっている。
			if (!isModelOrAccessoryOrEffect && !isCameraSelected && !isEnvironmentSelected)
			{
				//Utilが不明???IEnumerableが使用されているコンテナークラス　
				//ミーフォさんの自作クラス　目的は不明
				//
				MessageBox.Show(this.ApplicationForm, Util.Bilingual
				(
				this.Scene.Language,
				"対象のモデル、カメラ、アクセサリ、またはエフェクトがありません。\r\n対象のキーフレームを選択してから実行してください。",
				"No target selected.\r\nPlease select one or more keyframes to apply some noise."
				), Util.Bilingual(this.Scene.Language, this.Text, this.EnglishText), MessageBoxButtons.OK, MessageBoxIcon.Information);

				e.Cancel = true;

				return;
			}

			//usingがわからん???　他ソースからの読み込みではない　→　http://urashita.com/archives/1825　簡単に言うと　例外処理強制実力実行版のようなもの
			//usingは最後まで実行される。そのため例外処理の記述が少なくて済む？
			//フォームを外部からの読み出しのようだ
			//上部宣言につかわれているかどうか確認。　→上部で宣言していない。
			//ここの書き方は普通の関数宣言のようだ
			//引数にフォームをクラスとして読み込み
			//各値をゲッターセッターのように採取しているようだ。
			//
			using (var f = new ApplyNoiseForm
			{
				//フォームの値を見たらそこから値を採取しているようだ。
				KeyFrameInterval	 = keyFrameInterval,
				KeyShiftWidth		 = keyShiftWidth,
				NoiseValueInterval	 = noiseValueInterval,
				NoiseValue			 = noiseValue,

				IsPositionEnabled		 = isModelOrAccessoryOrEffect || isCameraSelected,
				IsPositionLocalVisible	 = isModelOrAccessoryOrEffect,
				IsRotationEnabled		 = isModelOrAccessoryOrEffect || isCameraSelected,
				IsRotationLocalVisible	 = isModelOrAccessoryOrEffect,
				IsEnvironmentEnabled	 = isEnvironmentSelected,
				IsPositionLocal			 = isPositionLocal,
				IsRotationLocal			 = isRotationLocal,
			})
			{
				//using関数中身
				//e.　が使われていないのが予想外。→中で使われている。
				//f　は　ApplyNoiseForm
				//キャンセル時の処理。
				if (f.ShowDialog() != DialogResult.OK)
				{
					e.Cancel = true;

					return;
				}

				keyFrameInterval	 = f.KeyFrameInterval;
				noiseValueInterval	 = f.NoiseValueInterval;
				noiseValue			 = f.NoiseValue;
				keyShiftWidth		 = f.KeyShiftWidth;
				isPositionLocal		 = f.IsPositionLocal;
				isRotationLocal		 = f.IsRotationLocal;

				//usingの中でusinが使われている　本気で意味分からん。→一応分かった　なんか幅広い例外処理みたいな感じ。
				//ディスシーンを引数にして状況を判断している
				//undoblocに入る事に不安が発生するが
				//その飛び先にてコメントアウトしているため心配は少なくて済む。
				//恐らくここで一旦引数に入れt状態を保存しているものだとおもわれる。
				using (new UndoBlock(this.Scene))
					switch (this.Scene.Mode)
					{
						//ActiveAccessory
						//Accessory   
						//現在選択中のアクセサリです。選択されていない場合は null が返ります。
						//つまりtrueで中に入るってこと？違う　this.Scene.Mode　=　EditMode.AccessoryModeってこと。
						case EditMode.AccessoryMode:
							var acc = this.Scene.ActiveAccessory;

							if (acc != null)//ここでチェック入れる意味がいまいち分からんけど。
								//LINQとforeachの組み合わせ　そして普通に出てくる_　分からん。
								//まずLINQのforeach構文をチェック
								//クエリ式：var q = from x in collection where x > 10 select x * x;
								//メソッド形式で LINQ：var q = collection.Where(x => x > 10).Select(x => x * x);

								//foreach (var 名 in 出席番号前半名)
								//{
								//	Console.Write("{0}\n", 名);
								//}

								//CameraLayer　
								//SelectedFrames　選択されたキーフレームです。
								//CameraSelectedFrameCollection

								//ProcessLayer　関数　

								//下記のファンクション
								//foreach　foreach(型名 変数 in コレクション)文
								foreach (var layer in acc.Layers.Where(_ => _.SelectedFrames.Any()))//これは何を対象にぶんまわすのか？//Layers　モーションレイヤ　例　Accessory.Layers　型 MotionLayerCollection アクセサリがもつモーションレイヤ
									ProcessLayer(r, layer.SelectedFrames.First().FrameNumber, layer.SelectedFrames.Last().FrameNumber, layer.Frames, isPositionLocal, isRotationLocal, null);

							break;
						case EditMode.CameraMode:
							var camera	= this.Scene.ActiveCamera;						//カメラの操作モードに切り替えるイメージ???　ActiveCamera　 ActiveCamera	 Camera	 現在選択中のカメラです。選択されていない場合は null が返ります。
							var env		= this.Scene.SelectedPropertyFrames;			// SelectedPropertyFrames	SelectedSceneFrameCollection	選択されているプロパティキーフレームにアクセスします。 

							if (camera != null)//別口でアクセス
								foreach (var layer in camera.Layers.Where(_ => _.SelectedFrames.Any()))　//ここのカメラはすぐ上でのカメラであることに注意。
									ProcessCameraLayer(r, layer.SelectedFrames.First().FrameNumber, layer.SelectedFrames.Last().FrameNumber, layer.Frames);

							if (env.Any())//たびたびでるanyは何なんだ。
								ProcessEnvironmentLayer(r, env.First().FrameNumber, env.Last().FrameNumber, this.Scene.PropertyFrames);

							break;
						case EditMode.EffectMode:
							var eff = this.Scene.ActiveEffect;

							if (eff != null)
								ProcessLayer(r, eff.SelectedFrames.First().FrameNumber, eff.SelectedFrames.Last().FrameNumber, eff.Frames, isPositionLocal, isRotationLocal, null);

							break;
						case EditMode.ModelMode:
							var model = this.Scene.ActiveModel;

							if (model != null)
								foreach (var bone in model.Bones)
									foreach (var layer in bone.Layers.Where(_ => _.SelectedFrames.Any()))
										ProcessLayer(r, layer.SelectedFrames.First().FrameNumber, layer.SelectedFrames.Last().FrameNumber, layer.Frames, isPositionLocal, isRotationLocal, bone);

							break;
					}
			}
		}

		//分からん
		//実際のプロセス項目のようだ
		//引数にとりあえず、フォームでの決定した値を全て入れ込み実行しているようだ。
		//この関数が実際に実行される場所は上記のシーン選択からのスイッチ文になる。


		//Random	r							ランダム値ランダムクラス
		//
		//long		beginFrame,					開始フレーム
		//long		endFrame,					終了フレーム
		//MotionFrameCollection	frames,			MotionFrameCollectionというクラスについて　キーの配列みたいなものかな
		//bool		isPositionLocal,			座標
		//bool		isRotationLocal,			回転
		//Bone		localBone					ローカルボーン　Bone　元は　model.Bones　マジvar勘弁　Scene.ActiveModel　ActiveModel	 Model	 現在選択中のモデルです。選択されていない場合は null が返ります。
		//プロセスレイヤー　うーん分からん。　フレームを実際に動かすのは別の関数。
		void ProcessLayer( Random r, long beginFrame, long endFrame, MotionFrameCollection frames, bool isPositionLocal, bool isRotationLocal, Bone localBone )
		{
			//varでクラスも入るのだ！　ProcessFramesは自作関数で下部に表記。
			//下部の書き方よく分からん　特に　_ => _.FrameNumber
			//下記はこのソース最難関部分　関数使用で引数にラムダ式にLINQを使っている！もう助けて！→　ラムダ式使ってなくね？　多分使ってる。　new HashSet<long>がそう　これが引数の値として
			//実行されていると予測する。
			//HashSet　に関してはMMMプラグインリファレンス検索にもかからなかった。ハッシュという概念がいまだによくわかっていない。
			//HashSet<T> はC#の標準関数のようだ。　→　順序は狂っても構わないので、 要素の検索だけ高速にできてほしい場合があって、 そういうコレクションのことをセット（set: もちろん、数学的な意味での集合のこと）と呼びます。
			//ラムダ付近の主語としては　何を返すのがポイントにあんる　そしてそのラムダに対してLINQをかけているのがまじで謎→直接はラムダは絡まなかった。
			//まずは問題を分離して｛｝の中身をじっくり読んでみる。特に何を返しているのかに注目して行う
			//現状の予測としては　値で返す？（配列チックな）いや違うLONｇだわ　そしlongに対してLINQを行っているのか？
			//LINQセレクトの基本構文　var mapped = src.Select(elem => elem * 2); // {6, 4, 18, 12}
			//ラムダ基本構文　Func<int, int, int> f =  (x, y) =>  {  return sum * prod; };
			var newKeyFrames = ProcessFrames(r, beginFrame, endFrame, keyFrameInterval, noiseValueInterval, new HashSet<long>(frames.Where(_ => _.Selected).Select(_ => _.FrameNumber) ) )//ProcessFramesの終わり
			.Select(_ =>//ここの最大の謎はSQLの条件搾りにラムダ式使ってるところかな。　あと(_ => _.Selected)の部分いるか？→これはselect文ではなく多分選択したキーというMMMのものであると推察　多分正解
			{//_は何を指すのか？ラムダ式ではなくLINQの一時的な式っぽい。これを勝手に今後エイリアスと呼ぶ事にしよう。
			 //でそのエイリアスはが今回呼び出す元になっている。　てっきりこれは引数かと思ってったんですけど　引数から値を取り出そうとしている。
			 //かなり意味がわからんぜよ！　いや分かった　。　その値をここの文でつくるんだ！　と思ったら　いきなり　｛｝の中の1行目からその値を使用しようとしている
			 //かなりループしている。それぞれの相当の理解が必要と理解した。
			 //
			 //ここのカッコ内はセレクト文の条件搾りっぽい？ここにくるには絞って絞ってみたいな感じ。
			 // FrameNumber				long				フレーム位置を取得・設定します。
			 // new HashSet<long>(frames.のframesは一体どこらから？　多分 SceneFrameCollection frames　（ProcessFrames）
				var baseFrame = frames.GetFrame(_.Key);								//frames　が自作関数なのか　MMM標準なのか分からない。→　引数だよ!　MotionFrameCollection
				var localRotation = GetLocalRotation(isRotationLocal, localBone, _.Value.RotationWidth, _.Value.RotationQuaternion);//何故ローカルローテーション

				baseFrame.FrameNumber = Math.Max(_.Key + GetNoise(r, keyShiftWidth), 0);
				baseFrame.Quaternion = isRotationLocal ? Quaternion.Multiply(baseFrame.Quaternion, _.Value.RotationQuaternion) : Quaternion.Multiply(_.Value.RotationQuaternion, baseFrame.Quaternion);
				baseFrame.Position += isPositionLocal ? GetLocalPosition(isPositionLocal, localBone, _.Value.PositionWidth, baseFrame.Quaternion) : _.Value.PositionWidth;

				baseFrame.Selected = true;

				return baseFrame;//リターンかましているからさすがにラムダ臭がする。
			})
			.ToArray(); //ここに注目　anyじゃないんかい！

			//http://qiita.com/RyotaMurohoshi/items/740151bd772889cf07de　LINQとラムダ。

			//foreach　１行書きみたいなのみたい。
			//_ => はLinqっぽい。
			foreach (var i in frames.Where(_ => _.Selected))
				frames.RemoveKeyFrame(i.FrameNumber);


			frames.AddKeyFrame(newKeyFrames.ToLookup(_ => _.FrameNumber)
			.Select(_ => _.Last())
			.ToList());
		}

		//分からん
		//Layerの概念が分かりません。
		//

		void ProcessEnvironmentLayer( Random r, long beginFrame, long endFrame, SceneFrameCollection frames )
		{
			//ここ超絶長い。まず関数を使用。そしてその関数の中でLINQ　そしてその関数の実行結果をLINQでさらに絞込み　そしてそのLINQの中で｛｝が入っている　その中身が文法的に分からない。
			var newKeyFrames = ProcessFrames(r, beginFrame, endFrame, keyFrameInterval, noiseValueInterval, new HashSet<long>(frames.Where(_ => _.Selected).Select(_ => _.FrameNumber)))
			.Select(_ =>
			{	//ここの{の意味が分からないがLINQの中を｛｝で囲む事ができるのか？
				var baseFrame = frames.GetFrame(_.Key);

				baseFrame.FrameNumber = Math.Max(_.Key + GetNoise(r, keyShiftWidth), 0);
				baseFrame.Gravity += _.Value.GravityWidth;
				baseFrame.GravityDirection += _.Value.GravityDirectionWidth;
				baseFrame.Selected = true;

				return baseFrame;
			})
			.ToArray();

			foreach (var i in frames.Where(_ => _.Selected))
				frames.RemoveKeyFrame(i.FrameNumber);

			frames.AddKeyFrame(newKeyFrames.ToLookup(_ => _.FrameNumber)
			.Select(_ => _.Last())
			.ToList());
		}

		//分からん
		//カメラレイヤーの操作時に使用するもの　通常のボーンと同じでは駄目。
		void ProcessCameraLayer( Random r, long beginFrame, long endFrame, CameraFrameCollection frames )
		{
			//ラムダ式というのは無名関数ということからひらめいた　恐らく　｛｝の中身は無名の関数なのだ。
			//ではその関数の中身の書き方。 何をどのような書き方なのかサッパリわかりんす　（メソッド構文）
			//だってここリターンまでつ勝てますよ。
			var existing = frames.ToList();
			var newKeyFrames = ProcessFrames(r, beginFrame, endFrame, keyFrameInterval, noiseValueInterval, new HashSet<long>(frames.Where(_ => _.Selected).Select(_ => _.FrameNumber)))
			.Select(_ =>
			{
				var baseFrame = frames.GetFrame(_.Key);

				baseFrame.FrameNumber = Math.Max(_.Key + GetNoise(r, keyShiftWidth), 0);
				baseFrame.Position += _.Value.PositionWidth;
				baseFrame.Angle += MathHelper.ToRadians(_.Value.RotationWidth);
				baseFrame.Selected = true;

				return baseFrame;
			})
			.ToArray();

			foreach (var i in frames.Where(_ => _.Selected))
				frames.RemoveKeyFrame(i.FrameNumber);

			frames.AddKeyFrame(newKeyFrames.ToLookup(_ => _.FrameNumber)
			.Select(_ => _.Last())
			.ToList());
		}

		//ProcessFrames　フレームを加工する？　きーを製作して設置

		//構文が相当分けわかめ　特に　<KeyValuePair<long, NoiseValue>> とかよくわからんし
		//IEnumerableからするとコルーチン？　しかしコルーチンでまわす意味が分からん。
		//→　IEnumerableはLINQで扱うための処理っぽい　インターフェース
		//
		//実際のキー操作をあつかうところっぽい
		//上部二つで使っているからこちらを先に読む。

		//KeyValuePair　→　検索で発見できない。
		//下部は日本語で書いてみる。
		//これはLINQで利用するため　IEnumerableをインターフェースを導入して
		//そのインターフェーすが2重ジェネリックって感じなのか　→違う　ラムダ式の文法
		//ヤッパリ　IEnumerable<KeyValuePair<long, NoiseValue>>の部分の理解のために　IEnumerableをもう少し詳しく調べてみる。
		//そもそも＜＞がジェネリックなのがか2度目の疑問。
		//自作コンテナの名前を思いっきり乗っけるっぽいね。
		//デリゲート分っぽいな！
		// HashSet<long>はラムダとか関係ないっぽいな。
		//IEnumerableを使う例文をとりあえず
		IEnumerable<KeyValuePair<long, NoiseValue>> ProcessFrames( Random r, long beginFrame, long endFrame, int keyFrameInterval, int noiseValueInterval, HashSet<long> existingFrames )
		{
			//ここの構文もとんでもなく分からん。＜＞で中身　型となんかのクラスでっせ。
			//それを宣言して　変数をつくあったんだろうけど　その中身に入るのが　なんなのか　currentFrame　なのか　=>　なのか分からん過ぎる。
			//NoiseValue　で宣言して引数いれてる？
			//ラムダ式例文
			//Func<int, int> square = x => x * x;
			//
			//Func<int, int, int> f =
			//( x, y ) =>
			//{
			//	int sum = x + y;
			//	int prod = x * y;
			//	return sum * prod;
			//};

			//currentFrameは引数を入れ込む変数　引数の対象名　これわざわざつけるのは意味あるのかな。
			//getNoiseValueはここで作った一時的変数　_に相当？　→　関数名
			//NoiseValueは何　→　他ﾍﾟｰｼﾞの自作クラス。　それに引数を入れていると思いきや（）が｛｝の悲しみ。
			// Func<int, int, int> Sum = (x, y) => x + y;

			//引数が1項不足している　それがわからんと思っていたがそれがラムダっぽい。Func<int, int, int>となっているうち、<>内の一番右が、戻り値の型になり、それ以外は引数の型になります。
			//
			//longはなんとビックリ帰り値の型ですわ。　そして何に変えるといえば　currentFrameにカエルッポイ。フレームもlongだからオッケーかな。

			//そして｛｝の中で処理を行っている

			//では例文にない　new　Noisevalueは？
			//これが処理の開始っぽい。配列としての入れ込み方法ですわ。だから｛｝

			//でここでgetNoiseValue関数が作製された　これを以下の文で普通に関数として使用する。という形。
			//んーん　普通に関数作ったほうがええんとちゃうんか・・・・
			//さらに申し上げれば　下記の場合　普通に引数のところで計算しているしなー　と思いきやそれを毎行するわけにも行かんからな。便利なのか？
			//currentFrameは引数です！　前のFUNCの。
			//ゲットノイズはオーバーロードが多いのです。
			Func<long, NoiseValue> getNoiseValue = currentFrame => new NoiseValue
			{
				//多分だけどラムダ式で　（）では無く｛｝だからね。
				PositionWidth			 = GetNoise(r, noiseValue.PositionWidth),
				RotationWidth			 = GetNoise(r, noiseValue.RotationWidth),
				GravityWidth			 = GetNoise(r, noiseValue.GravityWidth),
				GravityDirectionWidth	 = GetNoise(r, noiseValue.GravityDirectionWidth),
			};

			var nextKeyFrame		 = beginFrame + keyFrameInterval;					//keyFrameIntervalは引数　多分keyFrameIntervalは感覚なので　beginFrameを足す必要がある。
			var nextNoiseFrame		 = beginFrame + noiseValueInterval;					//noiseValueIntervalは引数

			var currentNoiseFrame	 = beginFrame;										//beginFrame
			var currentNoiseValue	 = getNoiseValue(currentNoiseFrame);				//ここらへん要る？
			var nextNoiseValue		 = getNoiseValue(nextNoiseFrame);					//ここらへん要る？　初期設計？

			//多分ここでの基礎はフレーム単位。
			//範囲内全て設置。
			for (var i = beginFrame; i <= endFrame; i++)
			{
				if (i >= nextNoiseFrame)
				{
					// update noise value　//ここが初期フレームと　エンドフレーム書き換えの原因である。
					//
					currentNoiseFrame = i;
					currentNoiseValue = nextNoiseValue;
					nextNoiseValue = getNoiseValue(nextNoiseFrame += noiseValueInterval);
				}

				if (keyFrameInterval == 0)//普通の中部分。
				{
					// overwrite exisitng key frame only//既存のキーフレームのみを上書き
					if (existingFrames.Remove(i))
						yield return new KeyValuePair<long, NoiseValue>(i, currentNoiseValue.Interpolate(nextNoiseValue, (float)(i - currentNoiseFrame) / (nextNoiseFrame - currentNoiseFrame)));
				}
				else
				if (i >= nextKeyFrame ||
				i == beginFrame)
				//次のキーフレームMaxと開始フレーム画が同じでも発動。
				{
					// create key frame　//0からキーフレムを製作するようだ。
					yield return new KeyValuePair<long, NoiseValue>(i, currentNoiseValue.Interpolate(nextNoiseValue, (float)(i - currentNoiseFrame) / (nextNoiseFrame - currentNoiseFrame)));

					if (i > beginFrame)
						nextKeyFrame += keyFrameInterval;
				}
			}
		}

		//ローカルのチェックボックス　イン時の差分と思われる。
		//ローカル　マトリックス
		static Matrix GetLocalMatrix( Bone bone )
		{
			if ((bone.BoneFlags & BoneType.LocalAxis) == 0 &&
			bone.LocalAxisX == Vector3.UnitX &&
			bone.LocalAxisY == Vector3.UnitY &&
			bone.LocalAxisZ == Vector3.UnitZ)
				return Matrix.Identity;

			return Matrix.LookAtLH(Vector3.Zero, bone.LocalAxisZ, bone.LocalAxisY);
		}

		//ローカルのチェックボックス　イン時の差分と思われる。
		//ローカルポジション
		static Vector3 GetLocalPosition( bool isLocal, Bone bone, Vector3 position, Quaternion quaternion )
		{
			if (!isLocal)
				return position;

			var rt = Vector3.Transform(bone == null ? position : Vector3.TransformCoordinate(position, GetLocalMatrix(bone)), quaternion);

			return new Vector3(rt.X, rt.Y, rt.Z);
		}

		//ローカルのチェックボックス　イン時の差分と思われる。
		//ローカルの回転
		static Quaternion GetLocalRotation( bool isLocal, Bone bone, Vector3 rotation, Quaternion quaternion )
		{
			if (!isLocal ||
			(bone.BoneFlags & BoneType.LocalAxis) == 0 &&
			bone.LocalAxisX == Vector3.UnitX &&
			bone.LocalAxisY == Vector3.UnitY &&
			bone.LocalAxisZ == Vector3.UnitZ)
				return quaternion;

			return Quaternion.Multiply(Quaternion.Multiply(Quaternion.RotationAxis(bone.LocalAxisY, rotation.Y), Quaternion.RotationAxis(bone.LocalAxisX, rotation.X)), Quaternion.RotationAxis(bone.LocalAxisZ, rotation.Z));
		}

		//ベクター３のノイズを製作（？）しかし引数にノイズで入っている。
		//これはRandomのクラスの形を理解しないと駄目？
		static Vector3 GetNoise( Random r, Vector3 width )
		{
			return new Vector3(GetNoise(r, width.X), GetNoise(r, width.Y), GetNoise(r, width.Z));
		}

		//ベクター３の少数点のノイズを製作（？）しかし引数にノイズが入っている。
		//しかし型がノイズである。　う～ん意味分からん。
		static float GetNoise( Random r, float width )
		{
			return (float)(r.NextDouble() * width - width / 2);
		}

		//イント値のランダム
		static int GetNoise( Random r, int width )
		{
			return r.Next(-width / 2, (int)(width / 2.0 + 0.5));
		}

		//インターフェース
		//ボタンに表示するアイコン画像です。大きさは32x32です。
		//nullを返すとデフォルト画像が表示されます。
		public override Image Image
		{
			get
			{
				return Resources.ApplyNoise32;
			}
		}

		//インターフェース
		//中コマンドバーのボタンに表示するアイコン画像です。大きさは32x32です。
		//nullを返すとデフォルト画像が表示されます。
		public override Image SmallImage
		{
			get
			{
				return Resources.ApplyNoise20;
			}
		}

		//インターフェース
		//プラグインの説明文です。
		//プラグインボタンのツールチップに表示されます。
		public override string Description
		{
			get
			{
				return "選択したキーフレームの移動および回転値に指定したオフセットを与えます。";
			}
		}

		//インターフェース
		//日本以外の環境で表示されるテキストです。
		//改行する場合は Environment.NewLineを使用してください。
		public override string EnglishText
		{
			get
			{
				return Util.EnvorinmentNewLine("Apply\r\nNoise");
			}
		}

		//インターフェース
		//日本語環境でボタンに表示されるテキストです。
		//改行する場合は Environment.NewLineを使用してください。
		public override string Text
		{
			get
			{
				return Util.EnvorinmentNewLine("ノイズ\r\n付加");
			}
		}

		//インターフェース
		//プラグインを一意に認識するためのGUIDです。
		//必ず設定してください。
		public override Guid GUID
		{
			get
			{
				return new Guid("3c92a157-b847-4eb6-9e2d-a1df9786dfcc");
			}
		}


		//インターフェースだが必須ではない

		//--Guid GUID { get; }
		//プラグインを一意に認識するためのGUIDです。
		//必ず設定してください。

		//Win32Window ApplicationForm { get; set; }
		//MMM本体フォームのHWND取得インターフェースです。
		//ShowDialog等に使用してください。

		//Scene Scene { get; set; }
		//Sceneオブジェクトです。
		//これを使用してMMM内部のオブジェクトにアクセスします。

		//string Description { get; }
		//プラグインの説明文です。
		//プラグインボタンのツールチップに表示されます。

		//--Image Image { get; }
		//ボタンに表示するアイコン画像です。大きさは32x32です。
		//nullを返すとデフォルト画像が表示されます。

		//--Image SmallImage { get; }
		//中コマンドバーのボタンに表示するアイコン画像です。大きさは32x32です。
		//nullを返すとデフォルト画像が表示されます。

		//--string Text
		//日本語環境でボタンに表示されるテキストです。
		//改行する場合は Environment.NewLineを使用してください。

		//--string EnglishText
		//日本以外の環境で表示されるテキストです。
		//改行する場合は Environment.NewLineを使用してください。

		//--void Run(CommandArgs e)
		//ボタンが押されたときに呼ばれます。
		//ここに処理を記述します。

		//void Dispose()
		//プラグインが破棄されるときに呼ばれます。
		//解放しないといけないオブジェクトがある場合は、ここで解放してください。
	}
}
