﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;

namespace Linearstar.MikuMikuMoving.Framework
{
	//Linq使用部分の本体。
	//ここは自作のコンテナークラス。
	//なんかノードのコピーとかやっている。
	//辞書などの複数の要素をひとつにまとめるクラスのことです。 複数の要素をまとめておく方法にはさまざまな方法があり、 その方法によって呼び名が変わります。 以下にコレクションの例とその簡単な説明を列挙します。

	//partial　部分型定義では、クラス、構造体、またはインターフェイスの定義を複数のファイルに分割することができます。
	static partial class Util
	{
		//IEnumerable
		//http://yohshiy.blog.fc2.com/blog-entry-211.html
		//IEnumerable インターフェース と同様に IEnumerable<T> は自作のコンテナークラス などで継承します。
		//どちらのインターフェースでも foreach は使えます。		しかし、 IEnumerable<T> の場合はそれに加えて大きなメリットがあります。
		//System.Linq を using しておくことによって、 LINQ で定義された多くの拡張メソッドが使用可能になることです。
		//あとイテレートの時に必要だったりする。


		//途中の<>に関して。これはジェネリック　つまり緩い型を作っている認識でいいのか？　そしてその命名は適当でもいいのか？→　書き換えるとエラーがでる
		//LinkedListNode　これはソースにない　MMMかと思いきやリファレンスがない。書き換えるとエラーがでる ホバーすると説明がデル　どうやらC#のものらしい
		//LinkedList<T>としてコレクションが上がる　http://ufcpp.net/study/dotnet/bcl_collection.html　配列っぽい。

		//まずNode<T> が意味不明。
		//出力される型は　IEnumerable<LinkedListNode<T>>ですよ。
		//引数はthis LinkedList<T> selfですよ。
		//thisってなんだよ。
		public static IEnumerable<LinkedListNode<T>> Nodes<T>(this LinkedList<T> self)
		{
			var rt = self.First;

			if (rt != null)
				do
					yield return rt;
				while ((rt = rt.Next) != null);
		}


		//Stream
		public static void CopyTo(this Stream self, Stream destination, int bufferSize)
		{
			var buf = new byte[bufferSize];
			var c = 0;

			while ((c = self.Read(buf, 0, buf.Length)) > 0)
				destination.Write(buf, 0, c);
		}


		//
		public static void CopyTo(this Stream self, Stream destination)
		{
			CopyTo(self, destination, 4096);
		}


		//
		public static string EnvorinmentNewLine(string s)
		{
			return s.Replace("\r\n", Environment.NewLine);
		}


		//
		public static bool IsEnglish(string lang)
		{
			return lang == "en";
		}


		//
		public static string Bilingual(string lang, string ja, string en)
		{
			return IsEnglish(lang) ? en : ja;
		}


		//
		public static object Member(this object self, string name, params object[] args)
		{
			var m = self.GetType().GetMember(name, BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic).FirstOrDefault();

			if (m is MethodInfo)
				return ((MethodInfo)m).Invoke(self, args);
			else if (m is PropertyInfo)
				return ((PropertyInfo)m).GetValue(self, args);
			else if (m is FieldInfo)
				return ((FieldInfo)m).GetValue(self);
			else
				return null;
		}

		//
		public static void SetMember(this object self, string name, object value, params object[] args)
		{
			var m = self.GetType().GetMember(name, BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic).FirstOrDefault();

			if (m is PropertyInfo)
				((PropertyInfo)m).SetValue(self, value, args);
			else if (m is FieldInfo)
				((FieldInfo)m).SetValue(self, value);
			else
				throw new InvalidOperationException();
		}
	}
}
