﻿using DxMath;
using Linearstar.MikuMikuMoving.Framework;

namespace Linearstar.MikuMikuMoving.ApplyKaidanPlugin
{
	//コンテナークラス

	//ノイズの値の保存のフォーマットみたいなもの
	//呼び出される時は
	//
	//NoiseValue noiseValue = new NoiseValue
	//{
	//	PositionWidth = new Vector3(5, 5, 5),
	//	RotationWidth = new Vector3(5, 5, 5),
	//	GravityWidth = 10,
	//	GravityDirectionWidth = new Vector3(1, 1, 1),
	//};

	//疑問１　入れ込みのときに親クラスの表記は必要ないのか？　（ゲッターセッターだから必要ない　それともコンテナクラスだから？）
	//疑問２　全部の要素埋めなくてもいいのか？だってゲッターセッターあるの5個あるんですけど。
	//疑問３　区切りが｛｝　これは配列の入れ込みのイメージでいいんだろうか

	/// <summary>
	/// 下記では６個　使用する時は4個
	/// ポジション				PositionWidth
	/// 回転					RotationWidth
	/// 重力					GravityWidth
	/// 重力影響				GravityDirectionWidth
	/// 回転クォータニオン		RotationQuaternion		入ってないし意味分からん
	///	内挿する　補間, 内挿	Interpolate				入ってないし意味分からん　中身はNoiseValueそのままなんだよなぁ。
	/// 
	/// </summary>

	//setは読み取り　//getは書き込み　これをするしないで強制宣言になるのか？
	//
	//public　int　prop｛
	//	get{return a;}
	//	set{a= Value;}
	//}

	//struct

	public struct KaidanValue
	{
		Quaternion rotationQuaternion;		//Quaternion DXmathにて使用　ところが DXmathがdllなので中身が分からないときたもんだ

		public Vector3 PositionWidth
		{
			get;
			set;
		}

		public Vector3 RotationWidth
		{
			get;
			set;
		}

		public float GravityWidth
		{
			get;
			set;
		}

		public Vector3 GravityDirectionWidth
		{
			get;
			set;
		}

		public Quaternion RotationQuaternion
		{
			get
			{
				if (rotationQuaternion != new Quaternion())
					return rotationQuaternion;

				var rot = MathHelper.ToRadians(this.RotationWidth);

				return rotationQuaternion = Quaternion.RotationYawPitchRoll(rot.Y, rot.X, rot.Z);
			}
		}
		//Interpolatek　単なる関数文
		public KaidanValue Interpolate(KaidanValue nextValue, float amount)
		{
			//参照し続けると、前のインスタンスを引き継ぐと
			return new KaidanValue
			{
				PositionWidth =				Vector3.Lerp(this.PositionWidth, nextValue.PositionWidth, amount),
				RotationWidth =				Vector3.Lerp(this.RotationWidth, nextValue.RotationWidth, amount),
				GravityWidth =				MathHelper.Lerp(this.GravityWidth, nextValue.GravityWidth, amount),
				GravityDirectionWidth =		Vector3.Lerp(this.GravityDirectionWidth, nextValue.GravityDirectionWidth, amount),
			};
		}
	}
}
